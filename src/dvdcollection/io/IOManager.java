/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdcollection.io;

import dvdcollection.model.DVD;
import dvdcollection.model.DVD.DVDRating;
import java.util.*;
import java.io.*;

/**
 * @author Tom
 * Class name: IOManager 
 * Provides all of the required functionality to read
 * and write a DVD collection data file. Class follows
 * the singleton pattern so that it may only be 
 * instantiated once and all clients must access it
 * through a common interface. 
 */
public class IOManager 
{
    /**
     * IOManager static instance variable. 
     */
    private static IOManager instance = null; 
    
    /**
     * Starts the IOManager class. Cannot be
     * called more than once during the 
     * application execution life cycle. 
     */
    public static void startup()
    {
        assert(instance == null);
        instance = new IOManager();
    }
    
    /**
     * Stops the IOManager class. Cannot be
     * called more than once during the 
     * application execution life cycle. 
     */
    public static void shutdown()
    {
        assert(instance != null);
        instance = null;
    }
    
    /**
     * Provides access to the IOManager
     * instance variable. 
     */
    public static IOManager getInstance()
    {
        assert(instance != null);
        return instance;
    }
    
    /**
     * Loads DVD collection from a file
     * into a list of DVD objects. 
     * @param file File to load collection from
     * @return List of DVD objects
     */
    public LinkedList<DVD> LoadDVDCollection(File file)
    {
        // Declare all variables needed to read in DVDs
        LinkedList<DVD> collection = new LinkedList<>();
        FileReader fReader = null;
        BufferedReader bReader = null;
        
        DVD dvd = null;
        DVDRating[] dvdRatings = DVD.DVDRating.values();
        
        // Initialise IO classes
        try
        {
            fReader = new FileReader(file);
        }
        catch (FileNotFoundException e)
        {
            System.err.println("Could find the collection file. Collection not loaded");
            return collection;
        }
        
        bReader = new BufferedReader(fReader);
        
        // Start reading
        String dvdLine = null;
        int line = 1;
        try
        {
            while ((dvdLine = bReader.readLine()) != null)
            {
                // Split the line on commas into tokens. 
                // We know that the tokens map as follows:
                // tokens[0] = Title                (String)
                // tokens[1] = Genre                (String)
                // tokens[2] = Title release year   (String)
                // tokens[3] = DVD release year     (String)
                // tokens[4] = Rating               (Integer, used to index an enum ordinal)
                // tokens[5] = Image path           (String)
                String[] tokens = dvdLine.split(",");
                
                if (tokens.length > 6)
                {
                    System.err.println("Too many tokens in DVD collection entry. Skipping line " + line + " in " + file.getName());
                }
                else if (tokens.length < 6)
                {
                    System.err.println("Too few tokens in DVD collection entry. Skipping line " + line + " in " + file.getName());
                }
                else if (tokens[0].isEmpty())
                {
                    System.err.println("DVD title not set. Skipping line " + line + " in " + file.getName());
                }
                else if (tokens[0].trim().length() == 0)
                {
                    System.err.println("DVD title not set. Skipping line " + line + " in " + file.getName());
                }
                else
                {
                    // Create DVD and add to collection
                    dvd = new DVD(tokens[0], tokens[1], tokens[2], tokens[3], DVDRating.valueOf(tokens[4]), tokens[5]);
                    collection.add(dvd);
                    
                    // Move to next line
                    ++line; 
                }
            }
        }
        catch (IOException e)
        {
            System.err.println("A general IOException occurred. Attempting to close file and return partial collection");
        }
        
        try
        {
            bReader.close();
        }
        catch (IOException e)
        {
            System.err.println("Could not close BufferedReader object");
        }
        
        try
        {
            fReader.close();
        }
        catch (IOException e)
        {
            System.err.println("Could not close collections file. Terminating program");
            System.exit(1);
        }
               
        return collection; 
    }
    
    /**
     * Writes a DVD collection to a file. 
     * @param file The file to write the collection to.
     * @param collection List of DVD objects to be saved. 
     */
    public void SaveDVDCollection(File file, LinkedList<DVD> collection)
    {
        // Declare all variables needed to write file
        BufferedWriter bWriter = null; 
        FileWriter fWriter = null;
        
        try
        {
            bWriter = new BufferedWriter(new FileWriter(file));
        }
        catch (IOException e)
        {
            System.err.println("Could not open file for writing. Application exiting");
            System.exit(1);
        }
        
        try
        {
            for (DVD dvd : collection)
            {
                // Write out dvd
                bWriter.write(dvd.toDatString() + "\n");
            }
            
            // Flush out buffer to disk
            bWriter.flush();
            bWriter.close();
        }
        catch (IOException e)
        {
            System.err.println("An error occurred while writing to file. Application exiting");
            System.exit(1);
        }
    }
}

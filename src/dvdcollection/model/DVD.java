/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdcollection.model;

/**
 * @author Tom
 * Class name: DVD
 * Data model for a DVD. Implements the
 * Comparable interface to allow DVDs
 * to be sorted by the built-in Java
 * sorting functions. 
 */
public class DVD implements Comparable<DVD>
{
     /**
     * DVD Ratings for Australian media
     * as defined by the OFLC. An unknown
     * rating is made available for 
     * international and unrated DVDs. 
     */
    public enum DVDRating
    {
        G,
        PG,
        M,
        MA15,
        R18,
        X18,
        Unknown,
    }
    
    /**
     * DVD Title
     */
    private String      title;
    
    /**
     * DVD Genre
     */
    private String      genre;                  
    
    /**
     * Year original DVD title was released
     */
    private String      titleReleaseYear;
    
    /**
     * Years DVD was released
     */
    private String      dvdReleaseYear;
    
    /**
     * DVD Rating
     */
    private DVDRating   rating;   
    
    /**
     * Path to DVD image
     */
    private String      imagePath;              // Either local or URL. 
    
    /**
     * Default constructor
     */
    public DVD()
    {
        this.title = "";
        this.genre = "";
        this.titleReleaseYear = "";
        this.dvdReleaseYear = "";
        this.rating = DVDRating.Unknown;
        this.imagePath = "";
    }
    
    /**
     * Constructor that takes all DVD details as arguments. 
     * @param title Title of the DVD
     * @param genre Genre of the DVD
     * @param titleReleaseYear Release year of the title
     * @param dvdReleaseYear Release year of the DVD
     * @param rating OFLC Rating for the DVD
     * @param imagePath Path to cover image, either local or URL
     */
    public DVD(String title, String genre, String titleReleaseYear, String dvdReleaseYear, DVDRating rating, String imagePath)
    {
        this.title = title;
        this.genre = genre;
        this.titleReleaseYear = titleReleaseYear;
        this.dvdReleaseYear = dvdReleaseYear;
        this.rating = rating;
        
        // An empty image path is read back
        // in as a single whitespace character,
        // we change imagePah to an empty 
        // string if this happens. 
        if (imagePath.equals(" "))
            this.imagePath = "";
        else
            this.imagePath = imagePath;
    }
    
    /**
     * Overridden toString function. 
     * @return DVD title as string. 
     */
    @Override
    public String toString()
    {
        return getTitle();
    }
    
    /**
     * Converts the data fields in a DVD into
     * a String suitable for writing to a DAT file. 
     * @return Processes string
     */
    public String toDatString()
    {
        String s = "";
        
        s += title + ",";
        s += genre + ",";
        s += titleReleaseYear + ",";
        s += dvdReleaseYear + ",";
        s += rating.toString() + ",";
       
        // We make the image path be a single
        // white space, if we don't there will
        // be issues reading the DVD back in. 
        if (imagePath.isEmpty())
            s += " ,";
        else
            s += imagePath + ",";
        
        return s;
    }
    
    // Getters and Setters
    
    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the genre
     */
    public String getGenre()
    {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre)
    {
        this.genre = genre;
    }

    /**
     * @return the titleReleaseYear
     */
    public String getTitleReleaseYear()
    {
        return titleReleaseYear;
    }

    /**
     * @param titleReleaseYear the titleReleaseYear to set
     */
    public void setTitleReleaseYear(String titleReleaseYear)
    {
        this.titleReleaseYear = titleReleaseYear;
    }

    /**
     * @return the dvdReleaseYear
     */
    public String getDvdReleaseYear()
    {
        return dvdReleaseYear;
    }

    /**
     * @param dvdReleaseYear the dvdReleaseYear to set
     */
    public void setDvdReleaseYear(String dvdReleaseYear)
    {
        this.dvdReleaseYear = dvdReleaseYear;
    }

    /**
     * @return the rating
     */
    public DVDRating getRating()
    {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(DVDRating rating)
    {
        this.rating = rating;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath()
    {
        return imagePath;
    }

    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }
    
    /**
     * Overidden compareTo function. Performs a string
     * compareTo function on two DVD titles to determine 
     * their ascending alphabetical order. 
     * @param o Other DVD to be compared.
     * @return Returns whether this DVD preceeds the other
     * DVD alphabetically. 
     */
    @Override
    public int compareTo(DVD o)
    {
        return this.title.compareTo(o.getTitle());
    }
}

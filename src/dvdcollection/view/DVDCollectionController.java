/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdcollection.view;

import dvdcollection.io.IOManager;
import dvdcollection.model.DVD;
import dvdcollection.model.DVD.DVDRating;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

/**
 * @author Tom
 * Class name: DVDCollectionController
 * Class provides all of the functionality
 * needed to handle changes in the DVD application user interface
 * and outputting of data to the user interface. 
 */
public class DVDCollectionController implements Initializable 
{
    /**
     * FXML attributes in the user interface.
     */
    @FXML
    private MenuItem loadCollectionMenuItem;    
    @FXML
    private MenuItem saveCollectionMenuItem;
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private MenuItem clearCollectionMenuItem;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private ComboBox<String> sortByComboBox;
    @FXML
    private ListView<DVD> titlesList;
    @FXML
    private TextField titleField;
    @FXML
    private TextField genreField;
    @FXML
    private TextField titleReleaseYearField;
    @FXML
    private TextField dvdReleaseYearField;
    @FXML
    private ComboBox<String> ratingComboBox;
    @FXML
    private Button loadImageBtn;
    @FXML
    private Button createDVDBtn;
    @FXML
    private Button editDVDBtn;
    @FXML
    private Button deleteDVDBtn;
    @FXML
    private Label coverPathLabel;
    @FXML
    private Label messageLabel;
    @FXML
    private ImageView coverImageFrame;
    @FXML
    private Button saveDVDBtn;
    @FXML
    private Label collectionTitleLabel;
    @FXML
    private MenuButton fileMenuButton;
    @FXML
    private MenuItem newCollectionMenuItem;
    
    /**
     * AppState enum is used to define the current
     * application state.
     * Default = Regular behaviour
     * CreatingDVD = User is inputting details for a new DVD
     * EditingDCD = User is editing the details of an existing DVD
     */
    private enum AppState
    {
        Default,
        CreatingDVD,
        EditingDVD,
    }
    
    /**
     * Application state variable. 
     */
    private AppState appState;
    
    /**
     * List of DVD objects that stores all of the
     * DVDs read in from a collection and any
     * DVDs subsequently added to the collection.
     */
    private LinkedList<DVD> collectionList; 
    
    /**
     * Name of the collection file open in the application. 
     */
    private String currentCollectionName;
    
    /**
     * Handle to collection file. 
     */
    private File collectionFile; 
    
    /**
     * List of genres in the current collection.
     */
    private LinkedList<String> genreList; 
    
    /**
     * Initialises the application and all 
     * necessary variables and resources. Also
     * defines two inline ChangeListener 
     * functions, one for listening to changes 
     * to the UI ListView object that displays
     * the DVDs to the user, and the other for
     * listening to the File menu being opened. 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        // Initialise app state
        appState = AppState.Default;
        
        // Initialise "Sort By" combo box items
        sortByComboBox.getItems().clear();
        sortByComboBox.getItems().add("All: A-Z");
        
        // Initialise "Rating" combo box items
        ratingComboBox.getItems().clear();
        ratingComboBox.getItems().add("G");
        ratingComboBox.getItems().add("PG");
        ratingComboBox.getItems().add("M");
        ratingComboBox.getItems().add("MA15+");
        ratingComboBox.getItems().add("R18+");
        ratingComboBox.getItems().add("X18+");
        ratingComboBox.getItems().add("Unknown");
        
        // Initialise collection list
        collectionList = new LinkedList<>();
        
        // Initialise genre list
        genreList = new LinkedList<>(); 
        
        // Listen for selection changes with a ChangeListener object (defined inline)
        titlesList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<DVD>() 
        {
            @Override
            public void changed(ObservableValue<? extends DVD> observable, DVD oldValue, DVD newValue) 
            {
                // Display DVD details
                displayDVDInfo(newValue);
                
                // Enable edit/delete button
                editDVDBtn.setDisable(false);
                deleteDVDBtn.setDisable(false);
            }
        });
        
        // Listen for the main menu button being open
        // Credit to http://stackoverflow.com/questions/18301635/how-can-i-detect-when-a-menubutton-open-its-menu-in-javafx 
        // for information on how to listen for a menu 
        // button being open as On Action would not work. 
        fileMenuButton.showingProperty().addListener(new ChangeListener<Boolean>() 
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) 
            {
                if(newValue) 
                {
                    // Disable saving if creating or editing a DVD
                    if (appState != AppState.Default)
                        saveCollectionMenuItem.setDisable(true);
                    else
                        saveCollectionMenuItem.setDisable(false);
                }
            }
        });
        
        // Startup IO Manager
        IOManager.startup();
    }
    
    /**
     * Handles the 'Exit' menu button being pressed. 
     * Causes the application to terminate. Any unsaved
     * DVDs or updates to DVDs will be lost on exit. 
     * @param event 
     */
    @FXML
    private void handleExitPressed(ActionEvent event)
    {
        // Shutdown the IOManager
        IOManager.shutdown();
        
        System.exit(0);
    }
    
    /**
     * Handles the "New Collection" menu button being pressed. 
     * Creates a new blank collection. 
     * @param event 
     */
    @FXML
    private void handleNewCollection(ActionEvent event)
    {
        // Clear the current collection
        collectionList.clear();
        
        // Clear file handle
        collectionFile = null;
        currentCollectionName = "";
        
        // Refresh UI
        refreshCollectionListView();
        collectionTitleLabel.setText(currentCollectionName);
        
        // Toggle the visibility of the create/save buttons
        createDVDBtn.setVisible(true);
        saveDVDBtn.setVisible(false);

        // Set usability of edit/delete buttons
        editDVDBtn.setDisable(true);
        deleteDVDBtn.setDisable(true);

        // Clear and disable info fields for editing
        clearDVDInfoFields();
        disableDVDInfoFields();
        
        // Clear sort by combo box
        refreshGenreList();
        repopulateSortByComboBox();
    }
    
    /**
     * Handles the "Load Collection" menu button being pressed. 
     * Displays an Open File Dialog to the user and proceeds
     * to load in a collection from the chosen file. 
     * @param event 
     */
    @FXML
    private void handleLoadCollection(ActionEvent event)
    {        
        // Create a file chooser         
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.dat)", "*.dat");
        fileChooser.getExtensionFilters().add(extFilter);
        
        // Show open file dialog and load contents of selected file
        collectionFile = fileChooser.showOpenDialog(null);
        if (collectionFile != null)
        {
            // Load collection
            collectionList.clear();
            collectionList = IOManager.getInstance().LoadDVDCollection(collectionFile);
            
            // Set loaded collection name
            currentCollectionName = collectionFile.getName();
            collectionTitleLabel.setText(currentCollectionName);
        
            // Refresh UI
            refreshCollectionListView();
        
            // Toggle the visibility of the create/save buttons
            createDVDBtn.setVisible(true);
            saveDVDBtn.setVisible(false);
            
            // Clear and disable info fields for editing
            clearDVDInfoFields();
            disableDVDInfoFields();
        
            // Refresh genres and repopulate sorting combo box
            refreshGenreList();
            repopulateSortByComboBox();
            
            // Sort list view
            sortTitlesList();
        }
    }
     
    /**
     * Handles the "Save Collection" menu button being pressed. 
     * Saves a collection to an existing file if the collection
     * was opened from a file. If the collection is a new one
     * a Save File Dialog is shown to the user so that they may
     * select where to save the collection to. 
     * @param event 
     */
    @FXML
    private void handleSaveCollection(ActionEvent event)
    {
        // If we don't already have a 
        // file open, create a new one. 
        if (collectionFile == null)
        {            
            // Create a file chooser    
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.dat)", "*.dat");
            fileChooser.getExtensionFilters().add(extFilter);
            
            collectionFile = fileChooser.showSaveDialog(null);
        }
        
        if (collectionFile != null)
        {
            IOManager.getInstance().SaveDVDCollection(collectionFile, collectionList);
        
            currentCollectionName = collectionFile.getName();
            collectionTitleLabel.setText(currentCollectionName);
        }
    }

    /**
     * Handles the "New DVD" button being pressed. 
     * Clears the DVD info input fields and enables
     * them to be edited. 
     * @param event 
     */
    @FXML
    private void handleNewDVDPressed(ActionEvent event)
    {
        // Set app state
        appState = AppState.CreatingDVD;
        
        // Toggle the visibility of the create/save buttons
        createDVDBtn.setVisible(false);
        saveDVDBtn.setVisible(true);
        saveDVDBtn.setDisable(true);
        
        // Toggle usability of edit/delete buttons
        editDVDBtn.setDisable(true);
        deleteDVDBtn.setDisable(true);
        
        // Clear info fields
        clearDVDInfoFields();
        
        // Enable info fields for editing
        enableDVDInfoFields();
        
        // Set the title field to have focus
        titleField.requestFocus();
    }
    
    /**
     * Handles the "Edit DVD" button being pressed. 
     * Loads the currently selected DVD info and 
     * enables the DVD info fields for editing. 
     * 
     * @param event 
     */
    @FXML
    private void handleEditDVDPressed(ActionEvent event)
    {        
        // Set app state
        appState = AppState.EditingDVD;
        
        // Toggle the visibility of the create/save buttons
        createDVDBtn.setVisible(false);
        saveDVDBtn.setVisible(true);
        
        // Toggle usability of edit/delete buttons
        deleteDVDBtn.setDisable(true);
        editDVDBtn.setDisable(true);
                
        // Enable info fields for editing
        enableDVDInfoFields();
        
        // Refresh genres and repopulate sorting combo box
        refreshGenreList();
        repopulateSortByComboBox();
        
        // Set the title field to have focus
        titleField.requestFocus();
    }

    /**
     * Handles the "Delete DVD" button being pressed. 
     * Removes the currently selected DVD from the
     * collection. 
     * @param event 
     */
    @FXML
    private void handleDeleteDVDPressed(ActionEvent event)
    {
        // Get the DVD to be deleted
        DVD dvd = titlesList.getSelectionModel().getSelectedItem();
        
        // Remove the dvd from the collection
        collectionList.remove(dvd);
        
        // Refresh genres and repopulate sorting combo box
        refreshGenreList();
        repopulateSortByComboBox();
        
        // Refresh UI
        clearDVDInfoFields();
        refreshCollectionListView();
    }
    
    /**
     * Handles the "Save DVD" button being pressed. 
     * Saves a new DVD into the collection. 
     * @param event 
     */
    @FXML
    private void handleSaveDVDPressed(ActionEvent event)
    {
        // Only allow saving if the current
        // DVD info fields are valid. 
        // TODO: Get rid of Validate function?
        // Theres plenty of other validation code scattered around
        //if (ValidateDVDInfoFields())
        {
            // Toggle the visibility of the create/save buttons
            createDVDBtn.setVisible(true);
            saveDVDBtn.setVisible(false);

            // Toggle usability of edit/delete buttons
            editDVDBtn.setDisable(false);
            deleteDVDBtn.setDisable(false);

            // Disable info fields for editing
            disableDVDInfoFields();
            
            // Save new DVD object
            if (appState == AppState.CreatingDVD)
                SaveNewDVD();
            else if (appState == AppState.EditingDVD)
                UpdateExistingDVD();
            
            // Refresh the UI
            refreshCollectionListView();
            
            // Refresh genres and repopulate sorting combo box
            refreshGenreList();
            repopulateSortByComboBox();
            
            // Alphabetise the titles list
            sortTitlesList();
            
            // Return app state to normal
            appState = AppState.Default;
        }
    }
    
    /**
     * Handles the "Load Image" button being pressed. 
     * Displays an Open File Dialog to the user and 
     * allows them to select a PNG, JPG or BMP file
     * to load to be used as the cover image for a
     * DVD. 
     * @param event 
     */
    @FXML
    private void handleLoadImage(ActionEvent event)
    {
        // Create a file chooser for image files 
        FileChooser fileChooser = new FileChooser();
        
        ArrayList<String> extensions = new ArrayList<>();
        extensions.add("*.jpg");
        extensions.add("*.bmp");
        extensions.add("*.png");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*bmp, *.jpg, *.png)", extensions);
        fileChooser.getExtensionFilters().add(extFilter);

        String imagePath = fileChooser.showOpenDialog(null).getPath();
        
        coverPathLabel.setText(imagePath);
        
        displayDVDImage(imagePath);
    }
    
    /**
     * Handles a selection change to the "Sort By"
     * combo box. Calls the appropriate sort function
     * for the current collection based on the selected
     * sort option in the "Sort By" combo box. 
     * @param event 
     */
    @FXML
    private void handleSortByChanged(ActionEvent event)
    {
        // Clear the DVD info fields
        clearDVDInfoFields();
        
        // Sort the titles list according to
        // the sort option selected in the
        // "Sort By" combo box. 
        sortTitlesList();
        
        // Disable edit and delete buttons
        // until a DVD is selected
        editDVDBtn.setDisable(true);
        deleteDVDBtn.setDisable(true);
    }
    
    /**
     * Handles the "Clear Collection" menu button being pressed.
     * Removes all of the DVDs in the current collection. 
     * @param event 
     */
    @FXML
    private void handleClearCollectionPressed(ActionEvent event)
    {
        collectionList.clear();
        
        refreshCollectionListView();
        
        // Toggle the visibility of the create/save buttons
        createDVDBtn.setVisible(true);
        saveDVDBtn.setVisible(false);

        // Toggle usability of edit/delete buttons
        editDVDBtn.setDisable(false);
        deleteDVDBtn.setDisable(false);

        // Clear and disable info fields for editing
        clearDVDInfoFields();
        disableDVDInfoFields();

        // Refresh genres and repopulate sorting combo box
        refreshGenreList();
        repopulateSortByComboBox();
    }
    
    /**
     * Handles input for the Title Release Year
     * and DVD Release Year fields. Only allows
     * numeric input and limits the years that 
     * can be entered to e from 1900 to 2099. 
     * @param event 
     */
    @FXML
    private void handleNumericField(KeyEvent event)
    {
        TextField textField;
        
        if (event.getSource().getClass() == TextField.class)
        {
            textField = (TextField)event.getSource();    
            String textFieldString;
            
            // Credit for regex that gets all characters on the
            // ascii table between SPACE and DELETE goes to the
            // answer at:
            // http://stackoverflow.com/questions/1444666/regular-expression-to-match-all-characters-on-a-u-s-keyboard
            // If we receive an ASCII character between SPACE and
            // DELETE on the ASCII table we want to compare the
            // current text field string + the new character. If
            // we receive some other character like backspace we
            // check the current value of the text field. This
            // means we are always checking the correct text field
            // value to see if it is alphanumeric. 
            if (event.getCharacter().matches("^[\\x20-\\x7F]*$") && event.getCode() != KeyCode.DELETE)
            {
                textFieldString = textField.getText() + event.getCharacter();
            }
            else
            {
                textFieldString = textField.getText();
            }
            
        
            // Credit for regex help to answer at:
            // http://stackoverflow.com/questions/10575624/java-string-see-if-a-string-contains-only-numbers-and-not-letters
            // Only allow 4 digit year numbers between 1900 and 2099 or an empty string
            if (!(textFieldString.matches("19[0-9]{2}") || textFieldString.matches("20[0-9]{2}")) && !textFieldString.isEmpty())
            {
                // Set the error scheme on the text field
                textField.getStyleClass().add("error");
                event.consume();
                
                // Disable the save button
                saveDVDBtn.setDisable(true);
            }
            else
            {
                textField.getStyleClass().remove("error");
                
                // Enable the save button
                saveDVDBtn.setDisable(false);
            }
        }
    }
    
    /**
     * Handles input into the DVD title field. Enforces
     * the title being at least one character long. 
     * @param event 
     */
    @FXML
    private void handleDVDTitleField(KeyEvent event)
    {
       TextField textField;
        
        if (event.getSource().getClass() == TextField.class)
        {
            textField = (TextField)event.getSource();    
            String textFieldString;
            
            // Credit for regex that gets all characters on the
            // ascii table between SPACE and DELETE goes to the
            // answer at:
            // http://stackoverflow.com/questions/1444666/regular-expression-to-match-all-characters-on-a-u-s-keyboard
            // If we receive an ASCII character between SPACE and
            // DELETE on the ASCII table we want to compare the
            // current text field string + the new character. If
            // we receive some other character like backspace we
            // check the current value of the text field. This
            // means we are always checking the correct text field
            // value to see if it is alphanumeric. 
            if (event.getCharacter().matches("^[\\x20-\\x7F]*$") && event.getCode() != KeyCode.DELETE)
            {
                textFieldString = textField.getText() + event.getCharacter();
            }
            else
            {
                textFieldString = textField.getText();
            }
            
            // The DVD title string is considered invalid if it is empty
            if (textFieldString.isEmpty() || textFieldString.trim().isEmpty())
            {
                // Set the error scheme on the text field
                textField.getStyleClass().add("error");
                event.consume();
                
                saveDVDBtn.setDisable(true);
            }
            else
            {
                textField.getStyleClass().remove("error");
                
                // Enable the save button
                saveDVDBtn.setDisable(false);
            }
        } 
    }
    
    /**
     * Enables DVD information fields
     * so that they can be used to
     * create a new DVD or edit an
     * existing one. 
     */
    private void enableDVDInfoFields()
    {
        titleField.setEditable(true);
        genreField.setEditable(true);
        titleReleaseYearField.setEditable(true);
        dvdReleaseYearField.setEditable(true);
        ratingComboBox.setDisable(false);
        loadImageBtn.setDisable(false);
    }
    
    /**
     * Disables DVD information fields
     * so that they cannot be edited. 
     */
    private void disableDVDInfoFields()
    {
        titleField.setEditable(false);
        genreField.setEditable(false);
        titleReleaseYearField.setEditable(false);
        dvdReleaseYearField.setEditable(false);
        ratingComboBox.setDisable(true);
        loadImageBtn.setDisable(true);
    }
    
    /**
     * Displays the currently selected 
     * DVD's information fields. 
     * @param dvd DVD to display
     */
    private void displayDVDInfo(DVD dvd)
    {
        // Must check for dvd being null, because
        // it is possible that when setting nothing
        // selected in the ListView (index -1) a
        // Changed event is fired but there is not
        // actual object selected. 
        if (dvd != null)
        {
            titleField.setText(dvd.getTitle());
            genreField.setText(dvd.getGenre());
            titleReleaseYearField.setText(dvd.getTitleReleaseYear());
            dvdReleaseYearField.setText(dvd.getDvdReleaseYear());
            ratingComboBox.getSelectionModel().select(dvd.getRating().ordinal());
            coverPathLabel.setText(dvd.getImagePath());
            displayDVDImage(dvd);
        }
    }
    
    /**
     * Clears all the DVD info fields 
     */
    private void clearDVDInfoFields()
    {
        titleField.setText("");
        genreField.setText("");
        titleReleaseYearField.setText("");
        dvdReleaseYearField.setText("");
        ratingComboBox.getSelectionModel().select(-1);
        coverPathLabel.setText("");
        coverImageFrame.setImage(null);
    }
    
    /**
     * Checks whether the data fields for
     * a new DVD are valid. If they aren't
     * the text box is highlighted red. 
     * @return True if all DVD fields are
     * valid, otherwise false. 
     */
    private boolean ValidateDVDInfoFields()
    {
        // Validate the contents of the DVD
        // For text fields check for empty 
        // strings and strings made up of all 
        // spaces. For combo boxes check for
        // a selected value. 
        
        if (titleField.getText().isEmpty() || 
            titleField.getText().trim().isEmpty())
        {
            return false;
        }
        
         if (genreField.getText().isEmpty() || 
             genreField.getText().trim().isEmpty())
        {
            return false;
        }
        
         if (titleReleaseYearField.getText().isEmpty() || 
             titleReleaseYearField.getText().trim().isEmpty())
        {
            return false;
        }
        
         if (dvdReleaseYearField.getText().isEmpty() || 
             dvdReleaseYearField.getText().trim().isEmpty())
        {
            return false;
        } 
        
        if (ratingComboBox.getSelectionModel().getSelectedIndex() == -1)
        {
            // TODO: Indicate invalid value in combo box
        }
        
        // TODO: Something with image field
        
        
        // If we reach here everything was fine
        return true; 
    }
    
    /**
     * Updates the observable list used
     * by the ListView in the UI to 
     * display the DVD titles. 
     */
    private void refreshCollectionListView()
    {
        titlesList.getSelectionModel().select(-1);
        ObservableList<DVD> oldItems = titlesList.getItems();
        oldItems.clear();
        oldItems.addAll(collectionList);
    }
    
    /**
     * Updates the observable list used 
     * by the ListView in the UI to 
     * display only DVDs of a specific genre
     * @param genre Genre to sort by
     */
    private void refreshCollectionListViewByGenre(String genre)
    {
        titlesList.getSelectionModel().select(-1);
        ObservableList<DVD> oldItems = titlesList.getItems();
        oldItems.clear();
        
        for(DVD dvd : collectionList)
        {
            if (dvd.getGenre().equalsIgnoreCase(genre))
            {
                oldItems.add(dvd);
            }
        }
    }
    
    /**
     * Saves a new DVD object to the collection
     */
    private void SaveNewDVD()
    {
        // Create DVD object
        DVDRating r = DVDRating.Unknown;
        if (ratingComboBox.getSelectionModel().getSelectedIndex() >= 0)
        {
            r = DVDRating.values()[ratingComboBox.getSelectionModel().getSelectedIndex()];
        }
        
        DVD dvd = new DVD(titleField.getText().trim(),
                          genreField.getText().trim(),
                          titleReleaseYearField.getText().trim(),
                          dvdReleaseYearField.getText().trim(),
                          r,
                          coverPathLabel.getText().trim());  

        // Add to collection list
        collectionList.add(dvd);
        
        // Refresh genres and repopulate sorting combo box
        refreshGenreList();
        repopulateSortByComboBox();
        
        // Set new DVD as selected index 
        // (Won't be highlighted in list view however)
        titlesList.getSelectionModel().select(dvd);
    }
    
    /**
     * Updates an existing DVD in the collection
     */
    private void UpdateExistingDVD()
    {
        // Get & update the existing DVD object            
        DVD dvd = titlesList.getSelectionModel().getSelectedItem();
        
        DVDRating r = DVDRating.Unknown;
        if (ratingComboBox.getSelectionModel().getSelectedIndex() >= 0)
        {
            r = DVDRating.values()[ratingComboBox.getSelectionModel().getSelectedIndex()];
        }

        dvd.setTitle(titleField.getText().trim());
        dvd.setGenre(genreField.getText().trim());
        dvd.setTitleReleaseYear(titleReleaseYearField.getText().trim());
        dvd.setDvdReleaseYear(dvdReleaseYearField.getText().trim());
        dvd.setRating(r);
        dvd.setImagePath(coverPathLabel.getText().trim());
        
        // Refresh genres and repopulate sorting combo box
        refreshGenreList();
        repopulateSortByComboBox();
    }
    
    /**
     * Displays the DVD cover image
     * @param dvd DVD to load image from
     */
    private void displayDVDImage(DVD dvd)
    {
        Image image = null;
        try
        {
            image = new Image(new FileInputStream(dvd.getImagePath()));
            coverImageFrame.setImage(image);
        }
        catch (FileNotFoundException e)
        {
            coverImageFrame.setImage(null);
            System.err.println("Could not find image file. Image file not loaded.");
        }
    }
    
    /**
     * Displays a cover image from a file path
     * @param path Path to image
     */
    private void displayDVDImage(String path)
    {
        Image image = null;
        try
        {
            image = new Image(new FileInputStream(path));
            coverImageFrame.setImage(image);
        }
        catch (FileNotFoundException e)
        {
            System.err.println("Could not find image file. Image file not loaded.");
        }
    }
    
    /**
     * Refreshes the list of genres
     * in the collection.
     */
    private void refreshGenreList()
    {
        // Clear the list
        genreList.clear();
        
        // Add the genres
        for(DVD dvd : collectionList)
        {
            String genre = dvd.getGenre();
            
            if (genre.equals("") || genre.trim().length() == 0)
            {
                if (!genreList.contains("Unknown"))
                {
                    genreList.add("Unknown");
                }
            }
            else if (!genreList.contains(genre.trim()))
            {
                genreList.add(genre.trim());
            }
        }
    }
    
    /**
     * Re-populates the "Sort By" combo box
     * with options for each of the genre 
     * types and a default "All DVDs A-Z"
     * option. 
     */
    private void repopulateSortByComboBox()
    {
        // Re-Sort genre list in ascending order
        Collections.sort(genreList);
        
        // Clear and repopulate combo box
        sortByComboBox.getItems().clear();
        
        // Add the regular all A-Z option
        sortByComboBox.getItems().add("All: A-Z");
        
        // Add all the genres
        for(String genre : genreList)
        {
            sortByComboBox.getItems().add("Genre: " + genre);
        }
    }
    
    /**
     * Sorts the titles in the ListView
     * in the UI in ascending alphabetical
     * order. 
     */
    private void sortTitlesList()
    {
        // Sort By A-Z selected
        Collections.sort(collectionList);
        
        int selectedIndex = sortByComboBox.getSelectionModel().getSelectedIndex();
        
        if (selectedIndex <= 0)
        {
            // Update the titles list view
            refreshCollectionListView();
        }
        else
        {
            int selectedGenreIndex = sortByComboBox.getSelectionModel().getSelectedIndex() - 1;
            refreshCollectionListViewByGenre(genreList.get(selectedGenreIndex));
        }
    }
}
